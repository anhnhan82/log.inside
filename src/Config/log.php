<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 11:45
 */
return [
    'log_database' => [
        'driver'   => 'mongodb',
        'host'     => env('LOG_DB_HOST', 'localhost'),
        'port'     => env('LOG_DB_PORT', 27017),
        'database' => env('LOG_DB_DATABASE', 'logs'),
        'username' => env('LOG_DB_USERNAME'),
        'password' => env('LOG_DB_PASSWORD'),
        'options'  => [
            'database' => env('LOG_DB_DATABASE_AUTH', 'admin') // sets the authentication database required by mongo 3
        ]
    ],
];
