<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/5/17
 * Time: 17:07
 */
return [
    'log_queue' => [
        'driver'   => 'rabbitmq',
        'host'     => env('RABBITMQ_HOST', 'docker_rabbit_1'),
        'port'     => env('RABBITMQ_PORT', 5672),
        'vhost' => env('RABBITMQ_VHOST', 'log-activity'),
        'user' => env('RABBITMQ_USER', 'test'),
        'password' => env('RABBITMQ_PASSWORD', 'test'),
        'queue_name'  => env('RABBITMQ_QUEUE', 'logs')
    ],
];