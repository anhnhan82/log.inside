<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 11:43
 */

namespace Inside\Log;

use Illuminate\Support\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        $this->handleConfigs();
        $this->publishes([
            __DIR__ . '/migrations' => $this->app->databasePath() . '/migrations'
        ], 'migrations');

        if ($this->app->runningInConsole()) {
            // Add command here
            $this->commands([

            ]);
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(){
        $arrRepoName = array(
            'ActivityRepository',
        );

        foreach ($arrRepoName as $repoName) {
            $this->app->bind("Inside\\Log\\Repositories\\Contracts\\{$repoName}Interface", "Inside\Log\\Repositories\\{$repoName}");
        }
    }

    private function handleConfigs()
    {
        $configPath = __DIR__ . '/Config/log.php';
        $this->publishes([$configPath => config_path('log-database.php')]);
        $this->mergeConfigFrom($configPath, 'database.connections');

        $configQueue = __DIR__ . '/Config/queue.php';
        $this->publishes([$configQueue => config_path('log-queue.php')]);
        $this->mergeConfigFrom($configQueue, 'queue.connections');

        if(env('CORE_LOG_QUERY', false) == true){
            DB::connection('log_database')->enableQueryLog();
        }
    }

}