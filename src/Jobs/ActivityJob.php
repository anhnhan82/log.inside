<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/29/17
 * Time: 06:34
 */

namespace Inside\Log\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Inside\Log\Repositories\Contracts\ActivityRepositoryInterface;

class ActivityJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $arrParams;

    public function __construct($arrParams)
    {
        $this->arrParams = $arrParams;
    }

    /**
     * Execute the job.
     *
     * @param  AudioProcessor  $processor
     * @return void
     */
    public function handle(ActivityRepositoryInterface $activityRepository){
        $activityRepository->create($this->arrParams);
    }

}