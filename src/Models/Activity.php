<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/29/17
 * Time: 06:18
 */

namespace Inside\Log\Models;


class Activity extends BaseModel
{
    protected $collection = 'activities';

    protected $primaryKey = '_id';

    protected $fillable = [
        'app', // Inside, GiaoHang
        'module',
        'controller',
        'action',
        'user_id',
        'object_id',
        'ip',
        'params',
        'created_at',
        'action_time',
    ];

    public function setUserIdAttribute($value){
        $this->attributes['user_id'] = (int)$value;
    }
    public function setObjectIdAttribute($value){
        $this->attributes['object_id'] = (int)$value;
    }
    public function setStatusAttribute($value){
        $this->attributes['status'] = (int)$value;
    }
    public function setaction_timeAttribute($value){
        $this->attributes['action_time'] = (int)$value;
    }
}