<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 14:53
 */

namespace Inside\Log\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BaseModel extends Eloquent
{
    protected $connection = 'log_database';
}