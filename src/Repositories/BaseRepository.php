<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 15:04
 */

namespace Inside\Log\Repositories;

use Inside\Log\Jobs\ActivityJob;
use Inside\Log\Models\BaseModel;
use Inside\Log\Repositories\Contracts\BaseRepositoryInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function findById($id)
    {
        return $this->model->where($this->model->getKeyName(), $id)->first();
    }

    public function create(array $params)
    {
        $model = $this->model->create($params);
        return $model;
    }

    public function update($id, array $params)
    {
        $model = $this->model->where('id', $id)->first();
        if(!$model){
            return null;
        }
        $model->fill($params)->save();

        return $model;
    }

    /**
     * Find model by condition
     * @param array $condition
     * @param null $fields
     * @param null $sort
     * @param int $limit
     * @param int $offset
     * @return mixed
     * Usage
     *   $condition = [];
     *   $condition[] = $this->createConditionField('user_id', $user_id, '=');
     *   $condition[] = $this->createConditionField('title', 'item', 'like');
     *   $model->find($condition, null, ['field' => 'score', 'dir' => 'desc']);
     */
    public function find(array $condition, $fields = null, $sort = null, $limit = -1, $offset = 0)
    {
        $query = $this->model;

        // Chẹck has condition
        if (count($condition)) {
            $query = $this->_buildCondition($query, $condition);
        }

        // Has field
        if (is_array($fields)) {
            $query->select($fields);
        }

        // Has sort
        if (is_array($sort)) {
            $field = $sort['field'];
            $dir = isset($sort['dir']) ? $sort['dir'] : 'desc';
            $query->orderBy($field, $dir);
        }

        // Has limit
        if ($limit > 0) {
            $query->take($limit);
        }

        // Has offset
        if ($offset > 0) {
            $query->skip($offset);
        }

        // Execute query
        return $query->get();
    }

    /**
     * Count item match condition
     * @param array $condition
     * @return mixed
     */
    public function findCount(array $condition){
        $query = $this->model;
        // Chẹck has condition
        if (count($condition)) {
            $query = $this->_buildCondition($query, $condition);
        }

        return $query->count();
    }

    protected function _buildCondition($query, array $condition){
        if (count($condition)) {
            $query = $query->where(function ($qr) use ($condition) {
                foreach ($condition as $key => $cond) {
                    if (is_a($cond, 'stdClass')) {
                        switch (strtolower($cond->operator)){
                            case 'wherein':
                                $arrValue = is_array($cond->value) ? $cond->value : [$cond->value];
                                $qr->whereIn($cond->field, $arrValue);
                                break;
                            case 'wherenotin':
                                $arrValue = is_array($cond->value) ? $cond->value : [$cond->value];
                                $qr->whereNotIn($cond->field, $arrValue);
                                break;
                            case 'whereor':
                                $qr->Where(function ($qra)use ( $cond){
                                    $qra->orWhere(function ($oqr) use ( $cond){
                                        //return $this->_buildCondition($oqr, $cond->conditions);

                                        foreach ($cond->conditions as $con){
                                            $value = $con->operator == 'like' ? '%' . $con->value . '%' : $con->value;
                                            //$oqr->orwhere($con->field, $con->operator, $value);
                                            $oqr->orwhere($con->field, $con->operator, $value);
                                        }
                                    });
                                });
                                break;
                            case 'wherebetween':
                                $arrValue = is_array($cond->value) ? $cond->value : [$cond->value];
                                $qr->whereBetween($cond->field, $arrValue);
                                break;
                            default:
                                $value = $cond->operator == 'like' ? '%' . $cond->value . '%' : $cond->value;
                                $qr->where($cond->field, $cond->operator, $value);
                                break;
                        }

                    } else {
                        $qr->where($key, $cond);
                    }
                }
            });
        }
        return $query;
    }

    /**
     * Create condition for filter
     * @param $name
     * @param $value
     * @param string $operator like, >, >=, <, <=, =
     * @return \stdClass
     */
    public function createCondition($name, $value, $operator = '='){
        $cond = new \stdClass;
        $cond->field = $name;
        $cond->value = $value;
        $cond->operator = $operator;

        return $cond;
    }

    public function queue(array $arrParam, $queue = 'default'){
        /*
        $arrConf = include(dirname(__FILE__) .'../../Config/queue.php');
        $arrConf = $arrConf['log_queue'];
        $connection = new AMQPStreamConnection($arrConf['host'], $arrConf['port'], $arrConf['user'], $arrConf['password'], $arrConf['vhost']);

        $channel = $connection->channel();
        $channel->queue_declare($queue, false, true, false, false);

        $arrParam['action_time'] = time();

        $data = json_encode($arrParam);
        if(empty($data)) $data = "Hello World!";
        $msg = new AMQPMessage($data,
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );

        $channel->basic_publish($msg, '', $queue);

        //echo " [x] Sent ", $data, "\n";

        $channel->close();
        $connection->close();

        return true;
        */
        $job = (new ActivityJob($arrParam))->onQueue($queue);
        return $this->dispatch($job);
    }
}