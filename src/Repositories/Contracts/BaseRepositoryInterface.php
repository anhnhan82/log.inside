<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/21/17
 * Time: 14:51
 */

namespace Inside\Log\Repositories\Contracts;

interface BaseRepositoryInterface
{
    public function all();

    /**
     * Create model
     * @param array $params
     * @return mixed
     */
    public function create(array $params);

    /**
     * Update model
     * @param $id
     * @param array $params
     * @return mixed
     */
    public function update($id, array $params);

    /**
     * Find by id
     * @param $id
     * @return mixed
     */
    public function findById($id);

    public function find(array $condition, $fields = null, $sort = null, $limit = -1, $offset = 0);

    /**
     * Count item match condition
     * @param array $condition
     * @return mixed
     */
    public function findCount(array $condition);

    /**
     * Create condition for filter
     * @param $name
     * @param $value
     * @param string $operator like, >, >=, <, <=, =
     * @return \stdClass
     */
    public function createCondition($name, $value, $operator = '=');
}