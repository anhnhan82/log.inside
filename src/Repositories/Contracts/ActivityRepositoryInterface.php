<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/29/17
 * Time: 06:21
 */

namespace Inside\Log\Repositories\Contracts;


interface ActivityRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Overwite create faction with queue
     * @param array $params
     * @param bool $queue
     * @return mixed
     */
    public function create(array $params, $queue = null);
}