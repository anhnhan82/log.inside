<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/29/17
 * Time: 06:22
 */

namespace Inside\Log\Repositories;


use Inside\Log\Jobs\ActivityJob;
use Inside\Log\Models\Activity;
use Inside\Log\Repositories\Contracts\ActivityRepositoryInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ActivityRepository extends BaseRepository implements ActivityRepositoryInterface
{

    use DispatchesJobs;

    public function __construct(Activity $model)
    {
        parent::__construct($model);
    }

    /**
     * Create log activity
     * @param array $params
     * @param null $queue Create log activity asynchronize
     * @return mixed|static
     */
    public function create(array $params, $queue = null){
        // Insert by queue
        if($queue){
            if(!isset($params['action_time'])){
                $params['action_time'] = time();
            }
            return $this->queue($params, $queue);
        }else{
            // Insert direct
            return $this->model->create($params);
        }

    }
}