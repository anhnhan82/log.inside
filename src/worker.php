<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 7/6/17
 * Time: 06:38
 */
include_once('vendor/autoload.php');
use PhpAmqpLib\Connection\AMQPStreamConnection;

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$arrConf = [
    'driver'    => 'mongodb',
    'host'      => 'docker_mongodb_1',
    'database'  => 'hasaki',
    //'username'  => 'root',
    //'password'  => 'secret',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];

$capsule->addConnection($arrConf);
$capsule->addConnection($arrConf, 'log_database');
$capsule->getDatabaseManager()->extend('mongodb', function($config)
{
    return new Jenssegers\Mongodb\Connection($config);
});

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

$arrQueueConf = include('Config/queue.php');
$arrQueueConf = $arrQueueConf['log_queue'];


$connection = new AMQPStreamConnection($arrQueueConf['host'], $arrQueueConf['port'], $arrQueueConf['user'], $arrQueueConf['password'], $arrQueueConf['vhost']);
$channel = $connection->channel();

$channel->queue_declare($arrQueueConf['queue_name'], false, true, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$mode = new \Inside\Log\Models\Activity();
$activityRepository = new \Inside\Log\Repositories\ActivityRepository($mode);

$callback = function($msg) use ($activityRepository){
    echo " [x] Received ", $msg->body, "\n";
    $arrParam = json_decode($msg->body, true);
    $activityRepository->create($arrParam);
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume($arrQueueConf['queue_name'], '', false, false, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();

?>