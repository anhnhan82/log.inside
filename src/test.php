<?php
/**
 * Created by PhpStorm.
 * User: nhanva
 * Date: 6/22/17
 * Time: 09:07
 */
include_once('vendor/autoload.php');

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Container\Container as Container;

$app = new Container();
$app->singleton('app', 'Illuminate\Container\Container');

$app->singleton('config', 'Illuminate\Config\Repository');

$capsule = new Capsule;

$arrConf = [
    'driver'    => 'mongodb',
    'host'      => 'docker_mongodb_1',
    'database'  => 'hasaki',
    //'username'  => 'root',
    //'password'  => 'secret',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];

$capsule->addConnection($arrConf);
$capsule->addConnection($arrConf, 'log_database');
$capsule->getDatabaseManager()->extend('mongodb', function($config)
{
    return new Jenssegers\Mongodb\Connection($config);
});

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

//$users = Capsule::table('users')->get();
//dd($users->toArray());

$mode = new \Inside\Log\Models\Activity();
$activity = new \Inside\Log\Repositories\ActivityRepository($mode);

$result = $activity->create([
    'module' => 'Test',
    'controller' => 'funcTest',
    'action' => 'actionTest',
    'user_id' => 1000001,
    'ip' => '127.0.0.1',
    'params' => [
        'param1' => 'param value',
        'param2' => 'param value',
    ]
], 'logs');

var_dump($result);